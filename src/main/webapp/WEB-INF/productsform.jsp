<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 10.09.2016
  Time: 20:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Products</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li ><a href="/customers">Заказчики</a></li>
                    <li class="active"><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<h1>Список товаров</h1>
<br>
<br>
<table class="table table-bordered">
    <thead>
    <th>Id поставщика</th>
    <th>Поставщик</th>
    <th>Id товара</th>
    <th>Товар</th>
    <th>Цена</th>
    <th></th>
    <th></th>
    </thead>
    <tbody>
    <c:forEach items="${products}" var="product">
        <tr>
            <td><c:out value="${product.vendId}"/></td>
            <td><c:out value="${product.vendName}"/></td>
            <td><c:out value="${product.prodId}"/></td>
            <td><c:out value="${product.prodName}"/></td>
            <td><c:out value="${product.prodPrice}"/></td>
            <td class="danger"><a href="/deleteProduct?prodId=<c:out value="${product.prodId}"/>" onclick="return confirm('Вы действительно хотите удалить этот товар?');">удалить товар</a></td>
            <td><a href="/updateProduct?prodId=<c:out value="${product.prodId}"/>">редактировать товар</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<a href="/addProduct"> <button type="button" class="btn btn-success">Добавить товар</button></a>

</body>
</html>

