<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 11.09.2016
  Time: 19:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Order</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<h1>Список товаров</h1>
<h2>Заказ № ${order.orderNum} от ${order.orderDate}</h2>
<h3>ID клиента-${order.custId}</h3>
<br>
<br>
<table class="table table-bordered">
    <thead>
    <th>Товар</th>
    <th>Количество</th>
    <th>Цена</th>
    </thead>
    <tbody>

    <c:forEach items="${order.orderItems}" var="item">
        <tr>
            <td><c:out value="${item.prodName}"/></td>
            <td><c:out value="${item.count}"/></td>
            <td><c:out value="${item.price}"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
