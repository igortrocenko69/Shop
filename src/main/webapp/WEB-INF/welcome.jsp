<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 02.10.2016
  Time: 14:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Welcome</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/shared_css_style.css">
    <link rel="stylesheet" type="text/css" href="/css/jcarousel.basic.css">


    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>

    <script type="text/javascript" src="/js/jcarousel.basic.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("h1").css("color", "blue");
            $('.jcarousel').jcarousel({
                animation: {
                    duration: 800,
                    easing:   'linear',
                    rtl: false,
                    interval: 4000,

                    target: '+=1',

            autostart: true
                }
            });
        });

    </script>
</head>
<body>

<p></p>
<p></p>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Главная</a></li>
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>

                </ul>
            </div>


        </div>
    </div>
</nav>
<h1 align="center">Welcome!</h1>

<div class="wrapper">
    <div class="jcarousel-wrapper">
        <div class="jcarousel">
            <ul>
                <li><img src="http://emagnat.ru/wp-content/uploads/rabota-s-postavshhikami-dlya-internet-magazina-2.jpg"  width="600" height="400" alt="">
                </li>
                <li>
                    <img src="http://www.beton-group.ru/getimg/292/213/crop/files/core/62_image.jpg" width="600"
                         height="400" alt="">
                </li>
            </ul>
        </div>
        <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
        <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        <p class="jcarousel-pagination">

        </p>
    </div>
</div>
</body>
</html>
