<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 03.09.2016
  Time: 22:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>updateCustomer</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<form name="updateProduct"
      method="post"
      action="/updateProduct">
    <h1>Редактирование товара</h1>
    <br>
    <br>
    <div class="form-group">
        <label for="vendId">ID поставщика</label>
        <input type="text" id="vendId" name="vendId" class="form-control" placeholder="<c:out value="${product.vendId}"/>">
    </div>
    <!--div class="form-group">

        <label for="prodId">ID товара</label>
        <input type="text" id="prodId" class="form-control" placeholder="<c:out value="${product.prodId}"/>">
    </div-->
    <div class="form-group">
        <label for="prodName">Название товара</label>
        <input type="text" id="prodName" name="prodName" class="form-control" placeholder="<c:out value="${product.prodName}"/>">
    </div>
    <div class="form-group">
        <label for="prodPrice">Цена</label>
        <input type="text" id="prodPrice" name="prodPrice" class="form-control" placeholder="<c:out value="${product.prodPrice}"/>">
    </div>
    <br>

    <button type="submit" class="btn btn-primary btn-lg">Редактировать</button>

    <!--   <table border="5">
           <tr>
               <td>VendorsID</td>
               <td><input type=textbox name="vendId" size="25" value="<c:out value="${product.vendId}"/>"> </td>

           </tr>
           <tr>
               <td>ProdId</td>
               <td><input type=textbox name="prodId" size="25" value="<c:out value="${product.prodId}"/>" > </td>
           </tr>
           <tr>
               <td>ProdName</td>
               <td><input type=textbox name="prodName" size="25" value="<c:out value="${product.prodName}"/>"> </td>
           </tr>
           <tr>
               <td>ProdPrice</td>
               <td><input type=textbox name="prodPrice" size="25" value="<c:out value="${product.prodPrice}"/>" > </td>
           </tr>
       </table>
       <br>
       <br>
       <input type=submit value="OK">-->
</form>
</body>
</html>

