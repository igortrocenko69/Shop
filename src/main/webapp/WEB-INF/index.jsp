<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 29.08.2016
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>First</title>
</head>
<body>

<table>
    <thead>
    <th>Sort</th>
    <th>Weight</th>
    <th>Color</th>
    </thead>
    <tbody>
    <c:forEach items="${apples}" var="apple">
        <tr>
            <td><c:out value="${apple.type}"/></td>
            <td><c:out value="${apple.weight}"/></td>
            <td><c:out value="${apple.color}"/></td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
