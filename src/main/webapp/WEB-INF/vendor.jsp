<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 04.09.2016
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Vendor</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<br>
<br>
<table  class="table table-bordered">
    <thead>
    <th>Id поставщика</th>
    <th>Поставщик</th>
    <th>Город</th>
    <th>Страна</th>
    </thead>
    <tbody>
    <tr>
        <td><c:out value="${vendor.vendId}"/></td>
        <td><c:out value="${vendor.vendName}"/></td>
        <td><c:out value="${vendor.vendCity}"/></td>
        <td><c:out value="${vendor.vendCountry}"/></td>
    </tr>
    </tbody>
</table>

</body>
</html>
