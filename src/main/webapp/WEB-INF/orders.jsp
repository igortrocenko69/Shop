<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 11.09.2016
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>OrdersByCustomer</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<h1>Список заказов по клиенту ${custName}</h1>
<br>
<br>
<table class="table table-bordered">
    <thead>
    <th>Номер</th>
    <th>Дата</th>
    <th>ID клиента</th>
    <th>Клиент</th>
    <th>Сумма</th>
    <th></th>
    <th></th>
    </thead>
    <tbody>
    <c:forEach items="${orders}" var="order">
        <tr>
            <td><c:out value="${order.orderNum}"/></td>
            <td><c:out value="${order.orderDate}"/></td>
            <td><c:out value="${order.custId}"/></td>
            <td><c:out value="${order.custName}"/></td>
            <td><c:out value="${order.orderSum}"/></td>


            <td><a href="/findOrderByOrderNum?orderNum=<c:out value="${order.orderNum}"/>" title="товары по заказу">список товаров</a></td>
            <td class="danger"><a href="/deleteOrder?orderNum=<c:out value="${order.orderNum}"/>" onclick="return confirm('Вы действительно хотите удалить этот заказ111?');" title="ты хорошо подумал?">удалить заказ</a></td>

        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
