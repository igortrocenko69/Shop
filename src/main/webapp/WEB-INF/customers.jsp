<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 10.09.2016
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Customers</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>

    <style>
        #country {
            background-color: RGB(100, 150, 16);
            color: white; /
        }
    </style>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<h1>Список клиентов</h1>
<br>
<br>
<table class="table table-bordered">
    <thead>
    <th>ID клиента</th>
    <th>Клиент</th>
    <th>Город</th>
    <th>Страна</th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    </thead>
    <tbody>
    <c:forEach items="${customers}" var="customer">
        <tr>
            <td><c:out value="${customer.custId}"/></td>
            <td><c:out value="${customer.custName}"/></td>
            <td><c:out value="${customer.custCity}"/></td>
            <td id="country"><c:out value="${customer.custCountry}"/></td>
            <td class="info"><a href="/ordersByCustId?custId=<c:out value="${customer.custId}"/>&custName=<c:out value="${customer.custName}"/>" title="заказы по клиенту">отобразить заказы</a></td>
            <td class="success"><a href="/updateCustomer?custId=<c:out value="${customer.custId}"/>">редактировать клиента</a></td>
            <td class="warning"><a href="/makeOrder?custId=<c:out value="${customer.custId}"/>&custName=<c:out value="${customer.custName}"/>">сделать заказ</a></td>
            <td class="danger"><a href="/deleteCustomer?custId=<c:out value="${customer.custId}"/>" onclick="return confirm('Вы действительно хотите удалить этого клиента? Будут удалены все его заказы!');">удалить клиента</a></td>

        </tr>
    </c:forEach>
    </tbody>
</table>
<br>
<br>
<a href="/addCustomer"> <button type="button" class="btn btn-success">Добавить клиента</button></a>
</body>
</html>
