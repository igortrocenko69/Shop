<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 02.09.2016
  Time: 20:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>addProduct</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<br>
<br>
<form name="addProduct"
      method="post"
      action="/addProduct">
    <h1>Добавление товара</h1>
    <br>
    <br>
    <div class="form-group">
        <label for="vendId">ID поставщика</label>
        <input type="text" id="vendId" name="vendId" class="form-control" placeholder="ID поставщика">
    </div>
    <!--div class="form-group">
        <label for="prodId">ID товара</label>
        <input type="text" id="prodId" class="form-control" placeholder="ID товара">
    </div-->
    <div class="form-group">
        <br>
        <label for="prodName">Название товара</label>
        <input type="text" id="prodName" name="prodName" class="form-control" placeholder="название товара">
    </div>
    <div class="form-group">
        <br>
        <label for="prodPrice">Цена</label>
        <input type="text" id="prodPrice" name="prodPrice" class="form-control" placeholder="цена">
    </div>
    <br>

    <button type="submit" class="btn btn-primary btn-lg">Отправить</button>

    <!-- <table>

         <tr>
             <td>VendorsID</td>
             <td><input type=textbox name="vendId" size="25" > </td>
         </tr>
         <tr>
             <td>ProdId</td>
             <td><input type=textbox name="prodId" size="25" > </td>
         </tr>
         <tr>
             <td>ProdName</td>
             <td><input type=textbox name="prodName" size="25" > </td>
         </tr>
         <tr>
             <td>ProdPrice</td>
             <td><input type=textbox name="prodPrice" size="25" > </td>
         </tr>
     </table>
     <input type=submit value="OK">-->
</form>
</body>
</html>

