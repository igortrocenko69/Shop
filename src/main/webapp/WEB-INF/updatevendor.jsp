<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 04.09.2016
  Time: 19:19
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>updateVendor</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<form name="updateVendor"
      method="post"
      action="/updateVendor">
    <h1>Редактирование поставщика</h1>
    <br>
    <br>
    <div class="form-group">
        <label for="vendName">Название поставщика</label>
        <input type="text" id="vendName" name="vendName" class="form-control" placeholder="<c:out value="${vendor.vendName}"/>">
    </div>
    <!--div class="form-group">
        <label for="vendId">ID поставщика</label>
        <input type="text" id="vendId" class="form-control" placeholder="<c:out value="${vendor.vendId}"/>">
    </div-->
    <div class="form-group">
        <label for="vendCity">Город</label>
        <input type="text" id="vendCity" name="vendCity" class="form-control" placeholder="<c:out value="${vendor.vendCity}"/>">
    </div>
    <div class="form-group">
        <label for="vendCountry">Страна</label>
        <input type="text" id="vendCountry" name="vendCountry" class="form-control" placeholder="<c:out value="${vendor.vendCountry}"/>">
    </div>
    <br>

    <button type="submit" class="btn btn-primary btn-lg">Редактировать</button>

  <!--  <table border="5">
        <tr>
            <td>Name</td>
            <td><input type=textbox name="vendName" size="25" value="<c:out value="${vendor.vendName}"/>"> </td>

        </tr>
        <tr>
            <td>ID</td>
            <td><input type=textbox name="vendId" size="25" value="<c:out value="${vendor.vendId}"/>" > </td>
        </tr>
        <tr>
            <td>City</td>
            <td><input type=textbox name="vendCity" size="25" value="<c:out value="${vendor.vendCity}"/>"> </td>
        </tr>
        <tr>
            <td>Country</td>
            <td><input type=textbox name="vendCountry" size="25" value="<c:out value="${vendor.vendCountry}"/>" > </td>
        </tr>
    </table>
    <br>
    <br>
    <input type=submit value="OK">-->
</form>
</body>
</html>
