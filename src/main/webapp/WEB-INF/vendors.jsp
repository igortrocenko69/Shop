<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 05.09.2016
  Time: 10:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Vendors</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li ><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li class="active"><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<h1>Список поставщиков</h1>
<br>
<br>
<table class="table table-bordered">
    <thead>
    <th>Id поставщика</th>
    <th>Поставщик</th>
    <th>Город</th>
    <th>Страна</th>
    <th></th>
    <th></th>
    <th></th>
    </thead>
    <tbody>
<c:forEach items="${vendors}" var="vendor">
    <tr>
        <td><c:out value="${vendor.vendId}"/></td>
        <td><c:out value="${vendor.vendName}"/></td>
        <td><c:out value="${vendor.vendCity}"/></td>
        <td><c:out value="${vendor.vendCountry}"/></td>
        <td><a href="/productsByVendor?vendId=<c:out value="${vendor.vendId}"/>" title="продукты по поставщику">отобразить продукты</a></td>
        <td class="danger"><a href="/deleteVendor?vendId=<c:out value="${vendor.vendId}"/>" onclick="return confirm('Вы действительно хотите удалить этого поставщика?');">удалить поставщика</a></td>
        <td><a href="/updateVendor?vendName=<c:out value="${vendor.vendName}"/>">редактировать поставщика</a></td>

    </tr>
</c:forEach>
    </tbody>
</table>
<br>
<br>
<a href="/addVendor"> <button title="добавить поставщика" type="button" class="btn btn-success">Добавить поставщика</button></a>
</body>
</html>
