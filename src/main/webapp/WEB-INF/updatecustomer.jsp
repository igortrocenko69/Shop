<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 04.09.2016
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>updateCustomer</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<form name="updateCustomer"
      method="post"
      action="/updateCustomer">
    <h1>Редактирование клиента</h1>
    <br>
    <br>
    <div class="form-group">
        <label for="custName">Название</label>
        <input type="text" id="custName" name="custName" class="form-control" value=" <c:out value="${customer.custName}"/>">
        <br>
        <!--label for="custId">ID клиента</label>
        <input type="text" id="custId" class="form-control" placeholder="<c:out value="${customer.custId}"/>">
        <br-->
        <label for="custCity">Город</label>
        <input type="text" id="custCity" name="custCity" class="form-control" value=" <c:out value="${customer.custCity}"/>">
        <br>
        <label for="custCountry">Страна</label>
        <input type="text" id="custCountry" name="custCountry" class="form-control" value=" <c:out value="${customer.custCountry}"/>">
    </div>
    <br>
    <br>
    <button type="submit" class="btn btn-primary btn-lg">Редактировать</button>

    <!-- <table border="5">
         <tr>
             <td>Name</td>
             <td><input type=textbox name="custName" size="25" value="<c:out value="${customer.custName}"/>"> </td>

         </tr>
         <tr>
             <td>ID</td>
             <td><input type=textbox name="custId" size="25" value="<c:out value="${customer.custId}"/>" > </td>
         </tr>
         <tr>
             <td>City</td>
             <td><input type=textbox name="custCity" size="25" value="<c:out value="${customer.custCity}"/>"> </td>
         </tr>
         <tr>
             <td>Country</td>
             <td><input type=textbox name="custCountry" size="25" value="<c:out value="${customer.custCountry}"/>" > </td>
         </tr>
     </table>
     <br>
     <br>
     <input type=submit value="OK">-->
</form>
</body>
</html>

