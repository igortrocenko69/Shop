<%--
  Created by IntelliJ IDEA.
  User: Игорь
  Date: 15.09.2016
  Time: 22:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>MakeOrder</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(":button").click(function () {
                var order = {};
                order.custId = $("#custId").val();
                order.custName=$("#custName").val();
                var items = [];
                $("#111 > tbody  > tr").each(function () {
                    $this = $(this)
                    var id = $this.attr("id");
                    var count = $this.find(":text").val();
                    // var count='5';
                    // var count = document.getElementById('count').value;
                    if (count != "") {
                        var orderItem = {};
                        orderItem.prodId = id;
                        orderItem.count = count;
                        items.push(orderItem);
                    }
                });
                order.orderItems = items;

                $.ajax({
                    url: "/makeOrder",
                    type: "POST", contentType: "application/json;charset=utf-8",
                    data: JSON.stringify(order),
                    success: function (data) {
                        alert("заказ создан")
                        window.location = "/ordersByCustId?custId=" + order.custId + "&custName=" + order.custName;
                    }
                })
            });
        });
    </script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/customers">Заказчики</a></li>
                    <li><a href="/products">Товары</a></li>
                    <li><a href="/vendors">Поставщики</a></li>
                    <li><a href="/welcome">На главную</a></li>
                </ul>
            </div>


        </div>
    </div>
</nav>
<h3>Клиент ${custName}</h3>
<input id="custId" type=hidden name="custId" value=${custId}>
<input id="custName" type=hidden name="custName" value=${custName}>
<br>
<br>
<table id="111" class="table table-bordered">
    <caption>Формирование заказа</caption>
    <thead>
    <th>Название</th>
    <th>Цена</th>
    <th>Заказ</th>

    </thead>
    <tbody>
    <c:forEach items="${products}" var="product">

        <tr id="${product.prodId}">
            <td><c:out value="${product.prodName}"/></td>
            <td><fmt:formatNumber var="price" value="${product.prodPrice*1.25}" maxFractionDigits="2"/>
                <c:out value="${price}"/></td>
            <td id="count"><input type=text name="count" size="25" value=""></td>

        </tr>
    </c:forEach>
    </tbody>
</table>
<button type="button" class="btn btn-success">Сделать заказ</button>
<p></p>
</body>
</html>


