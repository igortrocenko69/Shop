package dao;


import model.Order;
import model.OrderItem;
import util.MyConnector;

import java.math.RoundingMode;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.DoubleAccumulator;


/**
 * @author Игорь Троценко
 *         This is a class for working with Orders
 */
public class OrderDAO {
    /**
     * Method for obtaining orders on range of dates
     *
     * @return list of orders on range of dates
     * @throws RuntimeException when some exception occurs
     */
    public List<Order> getOrderInfo(java.util.Date start, java.util.Date end) {  //вывести заказы за укзанный период
        String strQuery = "select * from orderscopy where order_date between ? and ?";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            statement.setDate(1, new java.sql.Date(start.getTime()));
            statement.setDate(2, new java.sql.Date(end.getTime()));
            ResultSet result = statement.executeQuery();
            List<Order> orderList = new ArrayList<>();

            while (result.next()) {
                Order order = new Order();
                order.setOrderNum(result.getInt("order_num"));
                order.setOrderDate(result.getDate("order_date"));
                order.setCustId(result.getInt("cust_id"));
                orderList.add(order);
            }
            return orderList;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error by run of method");
        }

    }

    /**
     * Method for obtaining order by number
     *
     * @return Order
     * @throws RuntimeException when some exception occurs
     * @throws NoSuchElementException  when order not found
     */

    public Order findOrderByNumber(int orderNumber) {                //найти заказ по номеру
        String strQuery = "select OIC.*,OC.order_date,OC.cust_id,prod_name,cust_name " +
                "from orderscopy as OC,orderitemscopy as OIC,productscopy as PC,custcopy as CC " +
                "where OIC.order_num=OC.order_num and CC.cust_id=OC.cust_id and OIC.prod_id=PC.prod_id and OC.order_num=?";


        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            statement.setInt(1, orderNumber);
            ResultSet result = statement.executeQuery();

            List<OrderItem> orderItems = new ArrayList<>();
            Order myOrder=null;

            if (result.next()) {
                myOrder = new Order();
                myOrder.setOrderNum(result.getInt("order_num"));
                myOrder.setOrderDate(result.getDate("order_date"));
                myOrder.setCustId(result.getInt("cust_id"));
                myOrder.setCustName(result.getString("cust_name"));

                orderItems.add(setValues(result));
            }

            while (result.next()) {
                orderItems.add(setValues(result));
            }
            if (myOrder != null) {
                myOrder.setOrderItems(orderItems);
            } else throw new NoSuchElementException("Order with number " + orderNumber + " not found");


            return myOrder;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing findOrderByNumber() method");
        }

    }

    /**
     * Method for obtaining orders by ID клиента
     *
     * @return List<Order>
     * @throws RuntimeException when some exception occurs
     * @throws NoSuchElementException  when order not found
     */

    public List<Order> findOrdersByCustomerId(int customerID) {   //найти заказы по ID клиента

        String strQuery = "select orderscopy.order_num,orderscopy.order_date,orderscopy.cust_id,custcopy.cust_name,sum(quantity*item_price) as order_sum" +
                " from orderscopy ,custcopy ,orderitemscopy " +
                " where orderscopy.order_num=orderitemscopy.order_num and orderscopy.cust_id=custcopy.cust_id and orderscopy.cust_id=?" +
                " group by order_num" +
                " order by order_num;";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            statement.setInt(1, customerID);
            ResultSet result = statement.executeQuery();

            List<Order> orders = new ArrayList<>();

            while (result.next()) {
                Order myOrder = new Order();
                myOrder.setOrderNum(result.getInt("order_num"));

                myOrder.setOrderDate(result.getDate("order_date"));

                myOrder.setCustId(result.getInt("cust_id"));
                myOrder.setCustName(result.getString("cust_name"));
                myOrder.setOrderSum(result.getDouble("order_sum"));
                orders.add(myOrder);

            }


            if (orders.isEmpty()) {
                throw new NoSuchElementException();
            }
            return orders;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing findOrdersByCustomerId() method");
        }

    }

    private OrderItem setValues(ResultSet result) throws SQLException {
        OrderItem orderItem = new OrderItem();
        orderItem.setProdId(result.getInt("prod_id"));
        orderItem.setProdName(result.getString("prod_name"));
        orderItem.setCount(result.getInt("quantity"));
        orderItem.setPrice(result.getDouble("item_price"));
        return orderItem;
    }

    /**
     * Method making of order
     *
     * @throws RuntimeException when some exception occurs
     */

    public void makeOrder(Order newOrder) {                          //создать заказ
        Connection dbConnection = null;
        PreparedStatement statement = null;
        int orderNumber = 0;


        try {
            dbConnection = MyConnector.getDBConnection();
            dbConnection.setAutoCommit(false);                      //начало транзакции

            try {
                statement = dbConnection.prepareStatement("INSERT INTO orderscopy(order_date, cust_id)\n" +
                        "VALUES(now(), ?);");
                statement.setInt(1, newOrder.getCustId());
                statement.executeUpdate();

                statement = dbConnection.prepareStatement("select max(order_num) as number\n" +
                        "from orderscopy;");                          //номер последнего заказа
                ResultSet result = statement.executeQuery();

                if (result.next()) {
                    orderNumber = result.getInt("number");
                }

                PreparedStatement orderItemsStatement = dbConnection.prepareStatement("INSERT INTO orderitemscopy(order_num, order_item, prod_id, quantity, item_price)\n" +
                        "VALUES( ? , ?, ?, ?, ?);");                 //добавляем элемент заказа
                PreparedStatement priceStatement = dbConnection.prepareStatement("select prod_price " +
                        "from productscopy " +                       //получаем цену из таблицы productscopy
                        "where prod_id=?;");
                int i = 0;


                for (OrderItem orderItem : newOrder.getOrderItems()) {   //добавляем эементы заказа в orderitemscopy
                    priceStatement.setInt(1, orderItem.getProdId());
                    result = priceStatement.executeQuery();


                    if (result.next()) {

                        orderItem.setPrice(result.getDouble("prod_price") * 1.25);
                    }


                    int j = 0;
                    orderItemsStatement.setInt(++j, orderNumber);
                    orderItemsStatement.setInt(++j, ++i);
                    orderItemsStatement.setInt(++j, orderItem.getProdId());
                    orderItemsStatement.setInt(++j, orderItem.getCount());
                    orderItemsStatement.setDouble(++j, orderItem.getPrice());
                    orderItemsStatement.addBatch();
                }
                orderItemsStatement.executeBatch();


                dbConnection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                dbConnection.rollback();
            }                                                         //конец транзакции




        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing makeOrder() method");
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }



    }

    /**
     * This method deletes an order from DB by unique identifier
     *
     * @param orderNumber identifier of order
     * @throws RuntimeException when some exception occurs or a foreign key constraint fails
     * @throws NoSuchElementException when order not found
     */

    public void deleteOrder(int orderNumber) {                    //удалить заказ

        PreparedStatement statement = null;

        try (Connection dbConnection = MyConnector.getDBConnection()) {

            statement = dbConnection.prepareStatement("select order_num from orderscopy where order_num=?");
            statement.setInt(1, orderNumber);
            ResultSet result = statement.executeQuery();

            if (!result.next()) {                                //проверка на наличие заказа
                throw new NoSuchElementException();
            }

            statement = dbConnection.prepareStatement("delete from orderscopy where order_num=?");
            statement.setInt(1, orderNumber);
            statement.executeUpdate();


        } catch (SQLIntegrityConstraintViolationException e) {
            throw new RuntimeException("Can't delete a row: a foreign key constraint fails");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing deleteOrder() method");
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }


}
