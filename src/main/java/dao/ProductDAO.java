package dao;

import model.Product;
import util.MyConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * @author Игорь Троценко
 *         This is a class for working with Products
 */
public class ProductDAO {

    /**
     * Method for obtaining all products
     *
     * @return list of all products
     * @throws RuntimeException when some exception occurs
     */
    public List<Product> getProducts() {         //вывести все продукты

        String strQuery = "select VS.vend_name,VS.vend_id,PS.prod_name,PS.prod_id,PS.prod_price from productscopy as PS,vendorscopy as VS where PS.vend_id=VS.vend_id order by prod_name;";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            ResultSet result = statement.executeQuery();
            List<Product> productList = new ArrayList<>();

            while (result.next()) {
                Product product = new Product();
                product.setVendName(result.getString("vend_name"));
                product.setProdId(result.getInt("prod_id"));
                product.setVendId(result.getInt("vend_id"));
                product.setProdName(result.getString("prod_name"));
                product.setProdPrice(result.getDouble("prod_price"));
                productList.add(product);
            }
            return productList;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing getProducts() method", e);
        }

    }

    /**
     * Method for obtaining products by ID of vendor
     *
     * @param vendorId identifier of vendor
     * @return list of products
     * @throws RuntimeException when some exception occurs
     */
    public List<Product> findProductByVendorId(int vendorId) {    //найти продукт по ID поставщика

        String strQuery = "select VS.vend_name,VS.vend_id,PS.prod_id,PS.prod_name,PS.prod_price from productscopy as PS,vendorscopy as VS where PS.vend_id=VS.vend_id and VS.vend_id=? order by prod_name;";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            statement.setInt(1, vendorId);

            ResultSet result = statement.executeQuery();
            List<Product> productList = new ArrayList<>();

            while (result.next()) {
                Product product = new Product();
                product.setVendName(result.getString("vend_name"));
                product.setVendId(result.getInt("vend_id"));
                product.setProdId(result.getInt("prod_id"));
                product.setProdName(result.getString("prod_name"));
                product.setProdPrice(result.getDouble("prod_price"));
                productList.add(product);

            }
            return productList;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing findProductByVendorId() method", e);
        }

    }

    /**
     * This method adding a product to DB
     *
     * @param product is Product
     * @throws RuntimeException when some exception occurs
     */

    public void addProduct(Product product) {                    //добавить продукт

        String strQuery = "insert into productscopy (prod_name,prod_price,vend_id) values(?,?,?)";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            int i = 0;
            statement.setString(++i, product.getProdName());
            statement.setDouble(++i, product.getProdPrice());
            statement.setInt(++i, product.getVendId());
            statement.executeUpdate();


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing addProduct() method");
        }
    }

    /**
     * This method deletes a product from DB by unique identifier
     *
     * @param productID identifier of product
     * @throws RuntimeException when some exception occurs or a foreign key constraint fails
     */

    public void deleteProduct(int productID) {                    //удалить продукт

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement("delete from productscopy where prod_id=? ")) {

            statement.setInt(1, productID);
            statement.executeUpdate();

        } catch (SQLIntegrityConstraintViolationException e) {
            throw new RuntimeException("Can't delete a row: a foreign key constraint fails");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing deleteProduct() method");
        }
    }

    /**
     * This method updates a product in DB
     *
     * @param product is Product
     * @throws RuntimeException when some exception occurs or a foreign key constraint fails
     */

    public void updateProduct(Product product) {                    //изменить продукт

        String strQuery = "update productscopy set prod_name=?,prod_price=?,vend_id=? where prod_id=?";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            int i = 0;
            statement.setString(++i, product.getProdName());
            statement.setDouble(++i, product.getProdPrice());
            statement.setInt(++i, product.getVendId());
            statement.setInt(++i, product.getProdId());
            statement.executeUpdate();

        } catch (SQLIntegrityConstraintViolationException e) {
            throw new RuntimeException("Can't update a field: a foreign key constraint fails");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing updateProduct() method");
        }
    }

    /**
     * Method for obtaining product by ID
     *
     * @param productId identifier of product
     * @return Product
     * @throws RuntimeException when some exception occurs
     * @throws NoSuchElementException when product not found
     */
    public Product findProductById(int productId) {    //найти продукт по ID

        String strQuery = "select VS.vend_name,VS.vend_id,PS.prod_id,PS.prod_name,PS.prod_price from productscopy as PS,vendorscopy as VS where PS.vend_id=VS.vend_id and PS.prod_id=? order by prod_name;";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            statement.setInt(1, productId);

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                Product product = new Product();
                product.setVendName(result.getString("vend_name"));
                product.setVendId(result.getInt("vend_id"));
                product.setProdId(result.getInt("prod_id"));
                product.setProdName(result.getString("prod_name"));
                product.setProdPrice(result.getDouble("prod_price"));
                return product;
            } else throw new NoSuchElementException();


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing findProductById() method", e);
        }

    }
}
