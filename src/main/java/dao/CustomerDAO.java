package dao;

import model.Customer;
import model.Vendor;
import util.MyConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * @author Игорь Троценко
 *         This is a class for working with Customers
 */


public class CustomerDAO {
    /**
     * Method for adding a customer in DB
     *
     * @param customer is Customer
     * @throws RuntimeException when some exception occurs
     */
    public void addCustomer(Customer customer) {                    //добавить заказчика
        String strQuery = "insert into custcopy (cust_name,cust_city,cust_country)  values (?,?,?) ";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {
            int i = 0;
            statement.setString(++i, customer.getCustName());
            statement.setString(++i, customer.getCustCity());
            statement.setString(++i, customer.getCustCountry());
            statement.executeUpdate();


        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing addCustomer() method");
        }

    }



    /**
     * This method deletes a customer from DB by unique identifier
     *
     * @param customerID identifier of customer
     * @throws RuntimeException when some exception occurs or a foreign key constraint fails
     *
     */
    public void deleteCustomer(int customerID) {                    //удалить заказчика

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement("delete from custcopy where cust_id=?")) {
            statement.setInt(1, customerID);
            statement.executeUpdate();
        } catch (SQLIntegrityConstraintViolationException e) {
            e.printStackTrace();
            throw new RuntimeException("Can't delete a row: a foreign key constraint fails");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing deleteCustomer() method");
        }

    }

    /**
     * This method updates a customer in DB
     *
     * @param customer is Customer
     * @throws RuntimeException when some exception occurs or a foreign key constraint fails
     */

    public void updateCustomer(Customer customer) {                    //изменить заказчика
        String strQuery = "update custcopy set cust_name=?,cust_city=?,cust_country=? where cust_id=? ";
        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            int i = 0;
            statement.setString(++i, customer.getCustName());
            statement.setString(++i, customer.getCustCity());
            statement.setString(++i, customer.getCustCountry());
            statement.setInt(++i,customer.getCustId());
            statement.executeUpdate();

        } catch (SQLIntegrityConstraintViolationException e) {
            throw new RuntimeException("Can't update a field: a foreign key constraint fails");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing updateCustomer() method");
        }

    }

    /**
     * Method for obtaining a customer by ID
     *
     * @param customerID is identifier of customer
     * @return Customer
     * @throws RuntimeException when some exception occurs
     * @throws NoSuchElementException  when customer not found
     */

    public Customer findCustomerById(int customerID) {    //найти клиента по ID

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement("select * from custcopy where cust_id=?")) {

            statement.setInt(1, customerID);

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                Customer customer = new Customer();
                customer.setCustId(result.getInt("cust_id"));
                customer.setCustName(result.getString("cust_name"));
                customer.setCustCity(result.getString("cust_city"));
                customer.setCustCountry(result.getString("cust_country"));
                customer.setCustAddress(result.getString("cust_address"));
                customer.setCustContact(result.getString("cust_contact"));
                customer.setCustEmail(result.getString("cust_email"));
                return customer;
            } else throw new NoSuchElementException();

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing findCustomerById() method");
        }
    }

    /**
     * Method for obtaining all customers
     *
     * @return list of customers
     * @throws RuntimeException when some exception occurs
     */

    public List<Customer> getCustomers() {         //вывести всеx клиентов

        String strQuery = "select cust_id, cust_name, cust_city, cust_country from custcopy";

        List<Customer> customerList = new ArrayList<>();

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Customer customer = new Customer();
                customer.setCustId(result.getInt("cust_id"));
                customer.setCustName(result.getString("cust_name"));
                customer.setCustCity(result.getString("cust_city"));
                customer.setCustCountry(result.getString("cust_country"));
                customerList.add(customer);
            }
            return customerList;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing getCustomers() method");
        }
    }
}


