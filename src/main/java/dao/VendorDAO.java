package dao;

import model.Vendor;
import util.MyConnector;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Игорь Троценко
 * This is a class for working with Vendors
 */
public class VendorDAO {

    /**
     * Method for adding a vendor in DB
     * @param vendor is object Vendors
     * @throws RuntimeException when some exception occurs
     */
    public void addVendor(Vendor vendor) {                    //добавить поставщика

        String strQuery="insert into vendorscopy (vend_name,vend_city,vend_country) values(?,?,?)";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            int i = 0;
            statement.setString(++i, vendor.getVendName());
            statement.setString(++i, vendor.getVendCity());
            statement.setString(++i, vendor.getVendCountry());
            statement.executeUpdate();

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing addVendor() method");
        }
    }

    /**
     * This method deletes a vendor from DB by unique identifier
     * @param vendorID identifier of vendor
     * @throws RuntimeException when some exception occurs or a foreign key constraint fails
     */

    public void deleteVendor(int vendorID) {                    //удалить поставщика

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement("delete from vendorscopy where vend_id=?")) {

            statement.setInt(1, vendorID);
            statement.executeUpdate();

        } catch (SQLIntegrityConstraintViolationException e) {
            throw new RuntimeException("Can't delete a row: a foreign key constraint fails");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing deleteVendor() method");
        }
    }

    /**
     * This method updates a vendor in DB
     * @param vendor is Vendor
     * @throws RuntimeException when some exception occurs or a foreign key constraint fails
     */

    public void updateVendor(Vendor vendor) {                    //изменить поставщика

        String strQuery="update vendorscopy set vend_name=?,vend_city=?,vend_country=? where vend_id=? ";

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            int i = 0;
            statement.setString(++i, vendor.getVendName());
            statement.setString(++i, vendor.getVendCity());
            statement.setString(++i, vendor.getVendCountry());
            statement.setInt(++i, vendor.getVendId());
            statement.executeUpdate();

        } catch (SQLIntegrityConstraintViolationException e) {
            throw new RuntimeException("Can't update a field: a foreign key constraint fails");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing updateVendor() method");
        }
    }

    /**
     * Method for obtaining all vendors
     * @return list of vendors
     * @throws RuntimeException when some exception occurs
     */

    public List<Vendor> getVendors() {         //вывести всеx поставщиков

        String strQuery="select vend_id, vend_name, vend_city, vend_country from vendorscopy";

        List<Vendor> vendorList = new ArrayList<>();

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement(strQuery)) {

            ResultSet result = statement.executeQuery();

            while (result.next()) {
                Vendor vendor = new Vendor();
                vendor.setVendId(result.getInt("vend_id"));
                vendor.setVendName(result.getString("vend_name"));
                vendor.setVendCity(result.getString("vend_city"));
                vendor.setVendCountry(result.getString("vend_country"));
                vendorList.add(vendor);
            }
            return vendorList;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing getVendors() method");
        }
    }

    /**
     * Method for obtaining a vendor by name
     * @return Vendor
     * @throws RuntimeException when some exception occurs
     * @throws NoSuchElementException when vendor not found
     */

    public Vendor findVendorByName(String vendorName) {    //найти поставщика по имени

        try (Connection dbConnection = MyConnector.getDBConnection();
             PreparedStatement statement = dbConnection.prepareStatement("select * from vendorscopy where vend_name=?")) {

            statement.setString(1, vendorName);

            ResultSet result = statement.executeQuery();

            if (result.next()) {
                Vendor vendor = new Vendor();
                vendor.setVendName(result.getString("vend_name"));
                vendor.setVendId(result.getInt("vend_id"));
                vendor.setVendAddress(result.getString("vend_address"));
                vendor.setVendCity(result.getString("vend_city"));
                vendor.setVendState(result.getString("vend_state"));
                vendor.setVendZip(result.getString("vend_zip"));
                vendor.setVendCountry(result.getString("vend_country"));
                return vendor;
            } else throw new NoSuchElementException();

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while executing findVendorByName() method");
        }
    }
}
