package servlets;

import dao.CustomerDAO;
import dao.VendorDAO;
import model.Customer;
import model.Vendor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Игорь on 04.09.2016.
 */
@WebServlet("/addVendor")
public class ServletAddVendor extends HttpServlet {
    private VendorDAO vendorDAO = new VendorDAO();
    private Vendor vendor = new Vendor();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/addvendor.jsp");
        dispatcher.forward(request, response);

    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();


        try {


            String vendorName = request.getParameter("vendName");

            if (vendorName != null && !vendorName.equals("")) {
                vendor.setVendName(vendorName);
            } else {
                throw new IllegalArgumentException("parametr vendName not found");
            }


            String vendCity = request.getParameter("vendCity");

            if (vendCity != null) {
                vendor.setVendCity(vendCity);
            }

            String vendCountry = request.getParameter("vendCountry");

            if (vendCountry != null) {
                vendor.setVendCountry(vendCountry);
            }


            vendorDAO.addVendor(vendor);
            response.sendRedirect("/vendors");


        } catch (Exception e) {
            e.printStackTrace();
            out.println(e);
        } finally {
            out.close();
        }

    }
}
