package servlets;

import dao.ProductDAO;
import model.Product;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Игорь on 30.08.2016.
 */
@WebServlet("/productsByVendor")
public class ServletProductsByVendor extends HttpServlet {
    private ProductDAO productDAO = new ProductDAO();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Product> products;

        String vendId = request.getParameter("vendId");

        products = productDAO.findProductByVendorId(Integer.parseInt(vendId));

        request.setAttribute("products", products);
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/products.jsp");
        dispatcher.forward(request, response);


    }
}
