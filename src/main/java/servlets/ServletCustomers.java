package servlets;

import dao.CustomerDAO;
import dao.VendorDAO;
import model.Customer;
import model.Vendor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Игорь on 10.09.2016.
 */
@WebServlet("/customers")
public class ServletCustomers extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Customer> customers;
        CustomerDAO customerDAO = new CustomerDAO();
        customers = customerDAO.getCustomers();

        request.setAttribute("customers", customers);
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/customers.jsp");
        dispatcher.forward(request, response);


    }
}
