package servlets;

import dao.CustomerDAO;
import model.Customer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by Игорь on 03.09.2016.
 */
@WebServlet("/addCustomer")
public class ServletAddCustomer extends HttpServlet {
    private CustomerDAO customerDAO = new CustomerDAO();
    private Customer customer = new Customer();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/addcustomer.jsp");
        dispatcher.forward(request, response);
    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();


        try {

            Map<String, String[]> inputParams = request.getParameterMap();


            String[] customerName = inputParams.get("custName");


            if (customerName != null && !customerName[0].equals("")) {
                customer.setCustName(customerName[0]);
            } else throw new IllegalArgumentException("parameter custName not found");


            String[] custCity = inputParams.get("custCity");

            if (custCity != null) {
                customer.setCustCity(custCity[0]);
            }


            String[] custCountry = inputParams.get("custCountry");

            if (custCountry != null) {
                customer.setCustCountry(custCountry[0]);
            }


            customerDAO.addCustomer(customer);
            response.sendRedirect("/customers");


        } catch (Exception e) {
            e.printStackTrace();
            out.println(e);
        } finally {
            out.close();
        }

    }
}
