package servlets;

import com.google.gson.Gson;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Order;
import model.Product;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Игорь on 11.09.2016.
 */
@WebServlet("/makeOrder")
public class ServletMakeOrder extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        BufferedReader reader = request.getReader();
        Gson gson = new Gson();

        Order order = gson.fromJson(reader, Order.class); //формирование объекта order из Json

        OrderDAO orderDAO = new OrderDAO();
        orderDAO.makeOrder(order);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String custId = request.getParameter("custId");
        String custName = request.getParameter("custName");
        ProductDAO productDAO = new ProductDAO();
        List<Product> products = productDAO.getProducts();
        request.setAttribute("custId", custId);
        request.setAttribute("custName", custName);
        request.setAttribute("products", products);
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/makeorder.jsp");
        dispatcher.forward(request, response);

    }

}
