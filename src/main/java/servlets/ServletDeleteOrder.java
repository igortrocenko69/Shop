package servlets;

import dao.OrderDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Игорь on 11.09.2016.
 */
@WebServlet("/deleteOrder")
public class ServletDeleteOrder extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        OrderDAO orderDAO = new OrderDAO();
        int orderNum = Integer.valueOf(request.getParameter("orderNum"));
        try {

            orderDAO.deleteOrder(orderNum);
            response.sendRedirect("/ordersByCustId");


        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            out.println(e);
        } finally {
            out.close();
        }
    }
}
