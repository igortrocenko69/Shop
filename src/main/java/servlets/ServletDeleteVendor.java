package servlets;

import dao.VendorDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Игорь on 04.09.2016.
 */
@WebServlet("/deleteVendor")
public class ServletDeleteVendor extends HttpServlet {
    private VendorDAO vendorDAO = new VendorDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String vendId = request.getParameter("vendId");
        try {
            if (vendId != null) {
                vendorDAO.deleteVendor(Integer.parseInt(vendId));
                response.sendRedirect("/vendors");

            } else throw new IllegalArgumentException("parameter vendId is not found ");

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            out.println(e);
        } finally {
            out.close();
        }
    }
}
