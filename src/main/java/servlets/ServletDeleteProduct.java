package servlets;

import dao.ProductDAO;
import model.Product;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Игорь on 03.09.2016.
 */
@WebServlet("/deleteProduct")
public class ServletDeleteProduct extends HttpServlet {
    private ProductDAO productDAO = new ProductDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);
    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        try {

            String prodId = request.getParameter("prodId");
            if (prodId != null) {
                productDAO.deleteProduct(Integer.parseInt(prodId));
                response.sendRedirect("/products");

            } else throw new IllegalArgumentException("parameter prodId is not found ");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            out.println(e);
        } finally {
            out.close();
        }


    }

}
