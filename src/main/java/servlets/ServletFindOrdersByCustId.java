package servlets;

import dao.OrderDAO;
import model.Order;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Игорь on 11.09.2016.
 */
@WebServlet("/ordersByCustId")
public class ServletFindOrdersByCustId extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);
    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        OrderDAO orderDAO = new OrderDAO();
        String customerID = request.getParameter("custId");
        String customerName = request.getParameter("custName");
        try {

            List<Order> orders = orderDAO.findOrdersByCustomerId(Integer.parseInt(customerID));

            request.setAttribute("orders", orders);
            request.setAttribute("custName", customerName);
            RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/orders.jsp");
            dispatcher.forward(request, response);

        } catch (NoSuchElementException e) {
            out.println("The orders by customer with ID  " + customerID + " is not found");
        } finally {
            out.close();
        }

    }
}
