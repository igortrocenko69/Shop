package servlets;

import dao.ProductDAO;
import model.Product;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by Игорь on 03.09.2016.
 */
@WebServlet("/updateProduct")
public class ServletUpdateProduct extends HttpServlet {
    private ProductDAO productDAO = new ProductDAO();
    private Product product = new Product();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String prodId = request.getParameter("prodId");
        product = productDAO.findProductById(Integer.parseInt(prodId));

        request.setAttribute("product", product);
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/updateproduct.jsp");
        dispatcher.forward(request, response);
    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {

            Map<String, String[]> inputParams = request.getParameterMap();


            String[] prodNames = inputParams.get("prodName");
            if (prodNames != null && !prodNames[0].equals("")) {
                product.setProdName(prodNames[0]);
            } else {
                throw new IllegalArgumentException("parameter prodName is not found");
            }

            String[] prodPrices = inputParams.get("prodPrice");
            if (prodPrices != null && !prodPrices[0].equals("")) {
                product.setProdPrice(Double.parseDouble(prodPrices[0]));
            } else {
                throw new IllegalArgumentException("parameter prodPrice is not found");
            }

            String[] vendIds = inputParams.get("vendId");
            if (vendIds != null && !vendIds[0].equals("")) {
                product.setVendId(Integer.parseInt(vendIds[0]));
            } else {
                throw new IllegalArgumentException("parameter vendId is not found");
            }


            productDAO.updateProduct(product);
            response.sendRedirect("/products");


        } catch (Exception e) {
            e.printStackTrace();
            out.println(e);
        } finally {
            out.close();
        }

    }
}
