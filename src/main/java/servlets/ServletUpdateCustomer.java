package servlets;

import dao.CustomerDAO;
import model.Customer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by Игорь on 04.09.2016.
 */
@WebServlet("/updateCustomer")
public class ServletUpdateCustomer extends HttpServlet {
    private CustomerDAO customerDAO = new CustomerDAO();
    private Customer customer = new Customer();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String customerID = request.getParameter("custId");
        customer = customerDAO.findCustomerById(Integer.parseInt(customerID));
        request.setAttribute("customer", customer);
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/updatecustomer.jsp");
        dispatcher.forward(request, response);


    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();


        try {


            String customerName = request.getParameter("custName");

            if (customerName != null && !customerName.equals("")) {
                customer.setCustName(customerName);
            } else {
                throw new IllegalArgumentException("parametr custName not found");
            }


            String custCity = request.getParameter("custCity");

            if (custCity != null) {
                customer.setCustCity(custCity);
            }


            String custCountry = request.getParameter("custCountry");

            if (custCountry != null) {
                customer.setCustCountry(custCountry);
            }


            customerDAO.updateCustomer(customer);
            response.sendRedirect("/customers");


        } catch (Exception e) {
            e.printStackTrace();
            out.println(e);
        } finally {
            out.close();
        }

    }
}
