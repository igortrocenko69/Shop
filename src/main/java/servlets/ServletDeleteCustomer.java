package servlets;

import dao.CustomerDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Игорь on 04.09.2016.
 */
@WebServlet("/deleteCustomer")
public class ServletDeleteCustomer extends HttpServlet {
    private CustomerDAO customerDAO = new CustomerDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String custId = request.getParameter("custId");
        try {
            if (custId != null) {
                customerDAO.deleteCustomer(Integer.parseInt(custId));
                response.sendRedirect("/customers");

            } else throw new IllegalArgumentException("parameter custId is not found ");

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            out.println(e);
        } finally {
            out.close();
        }
    }
}
