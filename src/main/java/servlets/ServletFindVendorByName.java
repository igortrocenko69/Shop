package servlets;

import dao.VendorDAO;
import model.Vendor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Игорь on 05.09.2016.
 */
@WebServlet("/findVendorByName")
public class ServletFindVendorByName extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        VendorDAO vendorDAO = new VendorDAO();
        String vendName = request.getParameter("vendName");
        try {

            Vendor vendor = vendorDAO.findVendorByName(vendName);

            request.setAttribute("vendor", vendor);
            RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/vendor.jsp");
            dispatcher.forward(request, response);

        } catch (NoSuchElementException e) {
            out.println("The vendor with name of " + vendName + " is not found");
        } finally {
            out.close();
        }

    }
}
