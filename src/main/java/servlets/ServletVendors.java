package servlets;

import dao.VendorDAO;
import model.Product;
import model.Vendor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Игорь on 04.09.2016.
 */
@WebServlet("/vendors")
public class ServletVendors extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        execute(request, response);

    }

    private void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        VendorDAO vendorDAO = new VendorDAO();
        List<Vendor> vendors = vendorDAO.getVendors();

        request.setAttribute("vendors", vendors);
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/vendors.jsp");
        dispatcher.forward(request, response);


    }
}
