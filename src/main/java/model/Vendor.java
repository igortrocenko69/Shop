package model;

/**
 * Created by Игорь on 24.07.2016.
 */
public class Vendor {
    private int vendId;
    private String vendName;
    private String vendAddress;
    private String vendCity;
    private String vendState;
    private String vendZip;
    private String vendCountry;

    public Vendor(int vendId, String vendName, String vendAddress, String vendCity, String vendState, String vendZip, String vendCountry) {
        this.vendId = vendId;
        this.vendName = vendName;
        this.vendAddress = vendAddress;
        this.vendCity = vendCity;
        this.vendState = vendState;
        this.vendZip = vendZip;
        this.vendCountry = vendCountry;
    }
    public Vendor(){};

    public int getVendId() {
        return vendId;
    }

    public void setVendId(int vendId) {
        this.vendId = vendId;
    }

    public String getVendName() {
        return vendName;
    }

    public void setVendName(String vendName) {
        this.vendName = vendName;
    }

    public String getVendAddress() {
        return vendAddress;
    }

    public void setVendAddress(String vendAddress) {
        this.vendAddress = vendAddress;
    }

    public String getVendCity() {
        return vendCity;
    }

    public void setVendCity(String vendCity) {
        this.vendCity = vendCity;
    }

    public String getVendState() {
        return vendState;
    }

    public void setVendState(String vendState) {
        this.vendState = vendState;
    }

    public String getVendZip() {
        return vendZip;
    }

    public void setVendZip(String vendZip) {
        this.vendZip = vendZip;
    }

    public String getVendCountry() {
        return vendCountry;
    }

    public void setVendCountry(String vendCountry) {
        this.vendCountry = vendCountry;
    }

    @Override
    public String toString() {
        return "model.Vendor{" +
                "vendId='" + vendId + '\'' +
                ", vendName='" + vendName + '\'' +
                ", vendAddress='" + vendAddress + '\'' +
                ", vendCity='" + vendCity + '\'' +
                ", vendState='" + vendState + '\'' +
                ", vendZip='" + vendZip + '\'' +
                ", vendCountry='" + vendCountry + '\'' +
                '}';
    }
}
