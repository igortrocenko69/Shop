package model;

/**
 * Created by Игорь on 24.07.2016.
 */
public class Product {

    private int vendId;
    private String vendName;
    private int prodId;
    private String prodName;
    private double prodPrice;
    private String prodDesc;

    public Product(int prodId, int vendId,String vendName, String prodName,double prodPrice, String prodDesc) {
        this.prodId = prodId;
        this.vendId = vendId;
        this.vendName=vendName;
        this.prodName = prodName;
        this.prodPrice = prodPrice;
        this.prodDesc = prodDesc;

    }

    public Product() {};

    public int getProdId() {
        return prodId;
    }

    public void setProdId(int prodId) {
        this.prodId = prodId;
    }

    public int getVendId() {
        return vendId;
    }

    public void setVendId(int vendId) {
        this.vendId = vendId;
    }

    public String getVendName() {
        return vendName;
    }

    public void setVendName(String vendName) {
        this.vendName = vendName;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public double getProdPrice() {
        return prodPrice;
    }

    public void setProdPrice(double prodPrice) {
        this.prodPrice = prodPrice;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }


    @Override
    public String toString() {
        return "Product{" +
                "vendId='" + vendId + '\'' +
                ", vendName='" + vendName + '\'' +
                ", prodId='" + prodId + '\'' +
                ", prodName='" + prodName + '\'' +
                ", prodPrice=" + prodPrice +
                ", prodDesc='" + prodDesc + '\'' +
                '}';
    }
}
