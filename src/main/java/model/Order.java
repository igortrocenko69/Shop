package model;

/**
 * Created by Игорь on 25.07.2016.
 */

import java.util.Date;
import java.util.List;

public class Order {
    private int orderNum;
    private Date orderDate;
    private int custId;
    private String custName;
    private double orderSum;
    private List<OrderItem> orderItems;


    public Order(int orderNum, Date orderDate, int custId, String custName, double orderSum) {
        this.orderNum = orderNum;
        this.orderDate = orderDate;
        this.custId = custId;
        this.custName = custName;
        this.orderSum = orderSum;

    }

    public Order() {
    }

    ;

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public double getOrderSum() {
        return orderSum;
    }

    public void setOrderSum(double orderSum) {
        this.orderSum = orderSum;
    }


    @Override
    public String toString() {
        return "Order{" +
                "orderNum=" + orderNum +
                ", orderDate=" + orderDate +
                ", custId='" + custId + '\'' +
                ", custName='" + custName + '\'' +
                ", orderSum=" + orderSum +
                ", orderItems=" + orderItems +
                '}';
    }
}
