package model;

/**
 * Created by Игорь on 30.07.2016.
 */
public class OrderItem {
    private int prodId;
    private String prodName;
    private int count;
    private double price;

    public OrderItem(int prodId,String prodName, int count, double price) {
        this.prodId=prodId;
        this.prodName = prodName;
        this.count = count;
        this.price = price;
    }

    public OrderItem(){}

    public int getProdId() {
        return prodId;
    }

    public void setProdId(int prodId) {
        this.prodId = prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "prodId='" + prodId + '\'' +
                ", prodName='" + prodName + '\'' +
                ", count=" + count +
                ", price=" + price +
                '}';
    }
}
