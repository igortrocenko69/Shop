package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Игорь on 24.07.2016.
 */
public class MyConnector {

    /**
     * Method gets connection with MySQL Server
     * @return object Connection
     * @throws ClassNotFoundException
     * @throws SQLException
     */

    public static Connection getDBConnection() throws ClassNotFoundException, SQLException {
        String username = "root";
        String password = "toor";
        String URL = "jdbc:mysql://localhost:3306/shop?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        Connection dbConnection = null;

        Class.forName("com.mysql.cj.jdbc.Driver");        //подключение драйвера jdbc

        dbConnection = DriverManager.getConnection(URL, username, password); //получение соединения
        return dbConnection;

    }

}
